import string, random

from selenium.webdriver.ie import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver

from demoqa.venv.testing.pageobjects.BasePage import BasePage
from demoqa.venv.testing.locators.SamplePageLocators import SamplePageElements


# Elements from the Sample page
class SamplePage(BasePage):

    @property
    def random_string_comment(length):
        s = string.ascii_lowercase + string.digits
        return ''.join(random.sample(s, 10))

    def insert_comment(self):
        element = self.driver.find_element(*SamplePageElements.COMMENT_FIELD)
        element.send_keys(self.random_string_comment)

    def insert_name(self):
        element = self.driver.find_element(*SamplePageElements.NAME_FIELD)
        element.send_keys("Test")

    def invalid_email_address(self):
        element = self.driver.find_element(*SamplePageElements.EMAIL_FIELD)
        element.send_keys("invalidemailaddress")

    def valid_email_address(self):
        element = self.driver.find_element(*SamplePageElements.EMAIL_FIELD)
        element.send_keys("validemail@email.com")

    def submit_button(self):
        element = self.driver.find_element(*SamplePageElements.POST_COMMENT)
        element.click()

    def check_comment(self):
        element = self.driver.find_element(*SamplePageElements.COMMENT_FIELD)
        if element.is_displayed():
            pass
        else:
            element.click()


