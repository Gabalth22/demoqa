import unittest
from selenium import webdriver

from demoqa.venv.testing.pageobjects.homepageElements import HomepageElementValidation
from demoqa.venv.testing.locators.HomepageLocators import samplepage_button
from demoqa.venv.testing.locators.url import base_url


class InvalidEmailSamplePageError(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get(base_url)

    def testErrorInvalidEmailSamplePage(self):
        main_page = HomepageElementValidation(self.driver)
        main_page.search