from selenium.webdriver.common.by import By


class SamplePageElements(object):
    COMMENT_FIELD = (By.ID, 'comment')
    NAME_FIELD = (By.ID, 'author')
    EMAIL_FIELD = (By.ID, 'email')
    POST_COMMENT = (By.ID, 'submit')
    VALID_COMMENT = (By.CLASS_NAME, 'comment_container group')
