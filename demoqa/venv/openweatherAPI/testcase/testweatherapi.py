import unittest
import requests
from selenium import webdriver
from demoqa.venv.openweatherAPI.locators.urlandkey import ApiUrl


class TestWeatherRestApi(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_weather_rest_api(self):
        self.driver.get(ApiUrl.url)

        rest = requests.get(ApiUrl.url)
        data = rest.json()
        print(rest)
        print(data)

        rest = requests.get(ApiUrl.url)
        data = rest.json()

        temp = int(data["main"]["temp"])
        humidity = int(data["main"]["humidity"])
        wind = int(data["wind"]["speed"])

        kelvin_temp = temp
        celsius_temp = (kelvin_temp - 273.15)

        print("The temperature in " + ApiUrl.city + " is " + str(celsius_temp))
        print("The pressure is " + str(humidity) +
              " and and the wind speed is " + str(wind) + "m/s")

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
