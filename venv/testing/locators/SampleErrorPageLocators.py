from selenium.webdriver.common.by import By


class SampleErrorPageLocator(object):
    ERROR_PAGE = (By.ID, 'error-page')
    BACK_BUTTON = (By.PARTIAL_LINK_TEXT, 'Back')
