import unittest

from selenium import webdriver

from demoqa.venv.testing.pageobjects.HomePage import MainPage
from demoqa.venv.testing.pageobjects.SamplePage import SamplePage
from demoqa.venv.testing.pageobjects.ErrorPage import ErrorPage
from demoqa.venv.testing.locators.url import base_url


class TestErrorSamplePage(unittest.TestCase):

# Prerequisites
    def setUp(self):
        self.browser = webdriver.Chrome()
        self.browser.get(base_url)

# Test Case steps
    def test_error_validation_invalid_email_sample_page(self):
        main_page = MainPage(self.browser)
        sample_page = SamplePage(self.browser)
        error_page = ErrorPage(self.browser)

        main_page.click_sample_page_button()
        sample_page.insert_comment()
        sample_page.insert_name()
        sample_page.invalid_email_address()
        sample_page.submit_button()

        self.browser.implicitly_wait(10)

        error_page.verify_error_page()
        error_page.back_button()

        sample_page.valid_email_address()
        sample_page.submit_button()
        sample_page.check_comment()


# Driver quits in order to shut down the instance
    def tearDown(self):
        self.browser.delete_all_cookies()
        self.browser.quit()



if __name__ == "__main__":
    unittest.main()
