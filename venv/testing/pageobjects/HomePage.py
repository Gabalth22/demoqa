from demoqa.venv.testing.pageobjects.BasePage import BasePage
from demoqa.venv.testing.locators.HomepageLocators import MainPageLocators


# Elements from the main page
class MainPage(BasePage):

    def click_sample_page_button(self):
        element = self.driver.find_element(*MainPageLocators.SAMPLE_PAGE_BUTTON)
        element.click()
