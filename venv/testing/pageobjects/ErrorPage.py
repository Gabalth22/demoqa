from demoqa.venv.testing.pageobjects.BasePage import BasePage
from demoqa.venv.testing.locators.SampleErrorPageLocators import SampleErrorPageLocator


# Elements from the Error Page
class ErrorPage(BasePage):
    def verify_error_page(self):
        element = self.driver.find_element(*SampleErrorPageLocator.ERROR_PAGE)
        element.click()

    def back_button(self):
        element = self.driver.find_element(*SampleErrorPageLocator.BACK_BUTTON)
        element.click()
